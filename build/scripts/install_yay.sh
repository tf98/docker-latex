#!/bin/bash

useradd -m -r -s /bin/bash aur
passwd -d aur
echo 'aur ALL=(ALL) ALL' > /etc/sudoers.d/aur
mkdir -p /home/aur/.gnupg
echo 'standard-resolver' > /home/aur/.gnupg/dirmngr.conf
chown -R aur:aur /home/aur
chown -R aur:aur /build
cd /build
sudo -u aur git clone --depth 1 https://aur.archlinux.org/yay.git
cd yay
sudo -u aur makepkg --noconfirm -si
